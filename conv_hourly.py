#!/usr/bin/env python
# coding: utf-8

# In[58]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
import math
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, classification_report
from pyhive import hive


# In[59]:


hostip = "10.55.11.92"
port = 10000
dbname = "yogeeta"                                                            
tablename_all = "mapping_table"
con = hive.connect(hostip,port,auth = 'NOSASL')
tab_info_all = dbname+"." + tablename_all

df_usr = ' SELECT * FROM ' + tab_info_all

df_usr = pd.read_sql_query(df_usr,con)

df_usr.rename(columns=lambda x: x[14:], inplace=True)
df_usr.tail()


# In[60]:


df_usr.auth_channel.value_counts()


# In[61]:


df_usr['user_id'] = df_usr['userid'].astype(str).astype(int)


# In[62]:


df_usr1 = df_usr[['user_id', 'created_date']]


# In[63]:


df_usr1.describe()


# In[ ]:





# In[64]:


hostip = "10.55.11.92"
port = 10000
dbname = "yogeeta"                                                            
tablename_all = "tutorial_started"
con = hive.connect(hostip,port,auth = 'NOSASL')
tab_info_all = dbname+"." + tablename_all

df_tuts = ' SELECT * FROM ' + tab_info_all

df_tuts = pd.read_sql_query(df_tuts,con)

df_tuts.rename(columns=lambda x: x[17:], inplace=True)
df_tuts.tail()


# In[65]:


df_tuts1= df_tuts.groupby(['user_id'])['tutorial_start'].sum()
df_tuts2 = df_tuts1.reset_index()
df_tuts2.head()


# In[66]:


df_tuts2.describe()


# In[ ]:





# In[67]:


hostip = "10.55.11.92"
port = 10000
dbname = "yogeeta"                                                            
tablename_all = "addcash_clicked"
con = hive.connect(hostip,port,auth = 'NOSASL')
tab_info_all = dbname+"." + tablename_all

df_adcsh = ' SELECT * FROM ' + tab_info_all

df_adcsh = pd.read_sql_query(df_adcsh,con)


df_adcsh.rename(columns=lambda x: x[16:], inplace=True)
df_adcsh = df_adcsh.fillna(0)
df_adcsh.tail()


# In[68]:


df_adcsh['user_id'] = df_adcsh['userid'].astype(str).astype(int)


# In[69]:


df_adcsh1 = df_adcsh.groupby(['user_id'])['ad_csh_btn_clk'].sum()
df_adcsh2 = df_adcsh1.reset_index()
df_adcsh2.head()


# In[70]:


df_adcsh2.ad_csh_btn_clk.value_counts()


# In[71]:


df_adcsh2.describe()


# In[ ]:





# In[72]:


hostip = "10.55.11.92"
port = 10000
dbname = "yogeeta"                                                            
tablename_all = "prac_game_played"
con = hive.connect(hostip,port,auth = 'NOSASL')
tab_info_all = dbname+"." + tablename_all

df_prac = ' SELECT * FROM ' + tab_info_all

df_prac = pd.read_sql_query(df_prac,con)

df_prac.rename(columns=lambda x: x[17:], inplace=True)
df_prac.tail()


# In[73]:


df_prac['user_id'] = df_prac['userid'].astype(str).astype(int)


# In[74]:


df_prac1 = df_prac.groupby(['user_id'])['practice_games'].count()
df_prac2 = df_prac1.reset_index()
df_prac2.head()


# In[75]:


df_prac2.describe()


# In[76]:


df_prac2.practice_games.value_counts()


# In[ ]:





# In[77]:


hostip = "10.55.11.92"
port = 10000
dbname = "yogeeta"                                                            
tablename_all = "user_device_final"
con = hive.connect(hostip,port,auth = 'NOSASL')
tab_info_all = dbname+"." + tablename_all

df_dev = ' SELECT * FROM ' + tab_info_all

df_dev = pd.read_sql_query(df_dev,con)

df_dev.rename(columns=lambda x: x[18:], inplace=True)
df_dev.tail()


# In[78]:


df_dev2 = df_dev[['user_id', 'launchprice']]


# In[79]:


df_dev2.describe()


# In[ ]:





# In[80]:


hostip = "10.55.11.92"
port = 10000
dbname = "samrat_ds"                                                            
tablename_all = "cp_hh"
con = hive.connect(hostip,port,auth = 'NOSASL')
tab_info_all = dbname+"." + tablename_all

df_hh = ' SELECT * FROM ' + tab_info_all

df_hh = pd.read_sql_query(df_hh,con)
#df_hh = pd.read_csv("/home/samrat_ds/hh.csv")
df_hh.rename(columns=lambda x: x[6:], inplace=True)
df_hh.tail()


# In[81]:


df_hh['finish_type']=df_hh['finish_type'].fillna(0)
df_hh['finish_type']=df_hh['finish_type'].astype(str).astype(float)
df_hh['userid']=df_hh['userid'].astype(str).astype(int)
df_hh['drop_probability']=df_hh['drop_probability'].astype(str).astype(float)
df_hh['threshold']=df_hh['threshold'].astype(str).astype(float)
df_hh.head()


# In[82]:


df_hh['drop_adh'] = (df_hh['drop_probability']>df_hh['threshold']) & (df_hh['finish_type']==2)
df_hh.head()


# In[83]:


df_hh_final = df_hh.groupby(['userid'])['drop_adh'].max()
df_hh_final = pd.DataFrame(df_hh_final)
df_hh_final = df_hh_final.reset_index()
df_hh_final.columns = ['user_id','drop_adh']
df_hh_final.head()


# In[ ]:





# In[ ]:





# In[84]:


df_usr2 = df_usr1.merge(df_tuts2, how='left', left_on='user_id', right_on='user_id')
df_usr2.head()


# In[85]:


df_usr3 = df_usr2.merge(df_adcsh2, how='left', left_on='user_id', right_on='user_id')
df_usr3.head()


# In[86]:


df_usr4 = df_usr3.merge(df_prac2, how='left', left_on='user_id', right_on='user_id')
df_usr4.head()


# In[87]:


df_usr4.describe()


# In[ ]:





# In[88]:


df_usr5 = df_usr4.merge(df_hh_final, how='left', left_on='user_id', right_on='user_id')
df_usr5.head()


# In[89]:


df_usr6 = df_usr5.merge(df_dev2, how='left', left_on='user_id', right_on='user_id')
df_usr6.head()


# In[90]:


df_usr6.describe()


# In[91]:


df_usr7= df_usr6[df_usr6['launchprice']>0].fillna(0)
df_usr7.head()


# In[92]:


df_usr8 = df_usr7[['user_id', 'tutorial_start', 'practice_games', 'ad_csh_btn_clk', 'launchprice', 'drop_adh' ]]

df_usr8.columns = ['user_id', 'tutorial_start', 'gid', 'add cash button clicked', 'mobile_price','drop_adherence']
df_usr8 = df_usr8.set_index('user_id')
df_usr8.head()


# In[93]:


df_usr8['drop_adherence']=df_usr8['drop_adherence'].replace(True, 1)
df_usr8['drop_adherence'] = df_usr8['drop_adherence'].replace(False, 0)
df_usr8.head()


# In[94]:


df_usr9 = df_usr7[['user_id']]


# In[95]:


import joblib
from xgboost import XGBClassifier
loaded_model = joblib.load('/data/ds_users/RC_CP/xg_cp_new_3.pkl')


# In[96]:


from sklearn import preprocessing

# doing standardization
scaler = preprocessing.StandardScaler()
#X = df_usr8.drop('user_id' , axis=1)
X = pd.DataFrame(scaler.fit_transform(df_usr8))
#X = pd.DataFrame(df_sub)
X.columns = ['tutorial_start', 'gid', 'add cash button clicked', 'mobile_price','drop_adherence']


# In[97]:


prob = loaded_model.predict(df_usr8)
df_usr9['conv'] = prob.tolist()
#df_publish = df_usr8[['user_id','prob']]
#df_publish.set_index("user_id", inplace=True)
#df_publish = df_publish.merge(u_rd,on='user_id',how='left')
#df_publish.set_index("user_id", inplace=True)
pred = pd.DataFrame(prob)
pred[0].value_counts()


# In[ ]:




